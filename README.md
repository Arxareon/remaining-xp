# ⚠ Remaining XP has moved to [GitHub](https://github.com/Arxareon/RemainingXP). Look for new updates there!
If you wish to support the future development of Remaining XP, you can now [Sponsor](https://github.com/sponsors/Arxareon) my work on GitHub as an option.

***Thank you for understanding!***

- - -
***A simple solution to a simple problem:*** Remaining XP tracks your XP gains & calculates the remaining XP value. It also offers a customizable XP display, text integration for the default XP bar, chat notifications tracking your Rested XP accumulation & more.

## Features
* Get more information on your XP status - how much XP you need exactly, how many Rested XP you have accumulated & more.
* Includes a fully customizable, repositionable and resizable optional custom XP bar, an optional enhancement of the default XP bar text, detailed XP tooltip, chat notifications, & much more!
* Set up Remaining XP via the Interface Options or the **/remxp** chat command *(some settings are not available as chat commands)*.
* Includes multi-version support for Retail, Classic and Burning Crusade Classic!

## Other Addons
*If you like minimalistic solutions, you might want to check out there Addons as well:*

[**Movement Speed**](https://bitbucket.org/Arxareon/movement-speed) • View the movement speed of anyone in customizable displays and tooltips with fast updates.

*Coming soon™:*
[**Party Targets**](https://bitbucket.org/Arxareon/party-targets) • See who your party and raid members are targeting.

*Coming soon™:*
[**RP Keyboard**](https://bitbucket.org/Arxareon/rp-keyboard) • Type fantasy letters & runes in chat to talk to other RP Keyboard users.

## Support
*Possibilities to directly support the development of Remaining XP and other addons:*

[![Donate on PayPal](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/PayPal.svg/124px-PayPal.svg.png)](https://www.paypal.com/donate/?hosted_button_id=Z4FSAFKA5LX98)

## Links
[**CurseForge**](https://www.curseforge.com/wow/addons/remaining-xp) • Should you prefer, support development by watching ads on CurseForge, and keep up to date via automatic updates via the CurseForge App.

[**Wago**](https://addons.wago.io/addons/remaining-xp) • You can also use Wago to get updates and support development by watching ads, and provide much appreciated continuous support via a Subscription.

*You can also get updates via **WoWUP** if you have Wago.io enabled as an addon provider!*

## License
All Rights Reserved unless otherwise explicitly stated.

- - -
Thank you for checking out Remaining XP!
If you have any comments, wishes or problems, please, don't be afraid to share them. GLHF!

*Arxareon*