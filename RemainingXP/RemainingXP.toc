## Interface: 90205 #Fallback to the Mainline version
## Title: Remaining XP
## Version: 2.0.5
## X-Day: 9
## X-Month: 7
## X-Year: 2022
## Author: Barnabas Nagy (Arxareon)
## X-License: All Rights Reserved
## Notes: Shows the amount of XP needed to reach the next level & much more.
## SavedVariables: RemainingXPDB, RemainingXPCS
## SavedVariablesPerCharacter: RemainingXPDBC, RemainingXPCSC

WidgetTools/WidgetTools.lua
RemainingXPLocalizations.lua
RemainingXP.lua